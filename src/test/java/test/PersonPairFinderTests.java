package test;

import static org.junit.Assert.assertEquals;

import algorithm.Criteria;
import algorithm.Person;
import algorithm.PersonPair;
import algorithm.PersonPairFinder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class PersonPairFinderTests {

  Person sue = new Person();

  Person greg = new Person();

  Person sarah = new Person();

  Person mike = new Person();

  @Before
  public void setup() {
    sue.name = "Sue";
    sue.birthDate = new Date(50, 0, 1);
    greg.name = "Greg";
    greg.birthDate = new Date(52, 5, 1);
    sarah.name = "Sarah";
    sarah.birthDate = new Date(82, 0, 1);
    mike.name = "Mike";
    mike.birthDate = new Date(79, 0, 1);
  }

  @Test
  public void Returns_Empty_Results_When_Given_Empty_List() {
    List<Person> list = new ArrayList<Person>();
    PersonPairFinder personPairFinder = new PersonPairFinder(list);

    PersonPair result = personPairFinder.find(Criteria.FURTHEST);
    assertEquals(null, result.youngestPerson);

    assertEquals(null, result.oldestPerson);
  }

  @Test
  public void Returns_Empty_Results_When_Given_One_Person() {
    List<Person> list = new ArrayList<Person>();
    list.add(sue);

    PersonPairFinder personPairFinder = new PersonPairFinder(list);

    PersonPair result = personPairFinder.find(Criteria.FURTHEST);

    assertEquals(null, result.youngestPerson);
    assertEquals(null, result.oldestPerson);
  }

  @Test
  public void Returns_Closest_Two_For_Two_People() {
    List<Person> list = new ArrayList<Person>();
    list.add(sue);
    list.add(greg);
    PersonPairFinder personPairFinder = new PersonPairFinder(list);

    PersonPair result = personPairFinder.find(Criteria.FURTHEST);

    assertEquals(sue, result.youngestPerson);
    assertEquals(greg, result.oldestPerson);
  }

  @Test
  public void Returns_Furthest_Two_For_Two_People() {
    List<Person> list = new ArrayList<Person>();
    list.add(mike);
    list.add(greg);

    PersonPairFinder personPairFinder = new PersonPairFinder(list);

    PersonPair result = personPairFinder.find(Criteria.CLOSEST);

    assertEquals(greg, result.youngestPerson);
    assertEquals(mike, result.oldestPerson);
  }

  @Test
  public void Returns_Furthest_Two_For_Four_People() {
    List<Person> list = new ArrayList<Person>();
    list.add(sue);
    list.add(sarah);
    list.add(mike);
    list.add(greg);
    PersonPairFinder personPairFinder = new PersonPairFinder(list);

    PersonPair result = personPairFinder.find(Criteria.FURTHEST);

    assertEquals(sue, result.youngestPerson);
    assertEquals(sarah, result.oldestPerson);
  }

  @Test
  public void Returns_Closest_Two_For_Four_People() {
    List<Person> list = new ArrayList<Person>();
    list.add(sue);
    list.add(sarah);
    list.add(mike);
    list.add(greg);

    PersonPairFinder personPairFinder = new PersonPairFinder(list);

    PersonPair result = personPairFinder.find(Criteria.CLOSEST);

    assertEquals(sue, result.youngestPerson);
    assertEquals(greg, result.oldestPerson);
  }

}
