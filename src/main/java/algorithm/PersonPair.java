package algorithm;

import java.util.ArrayList;
import java.util.List;

public class PersonPair {

  public PersonPair() {
  }

  public PersonPair(Person onePerson, Person otherPerson) {
    this.setPair(onePerson, otherPerson);
    this.setAgeDifference();
  }

  public Person youngestPerson;

  public Person oldestPerson;

  public long ageDifference;

  public long getAgeDifference() {
    return ageDifference;
  }

  private void setPair(Person onePerson, Person otherPerson) {
    if (onePerson.isYoungerThan(otherPerson)) {
      this.youngestPerson = onePerson;
      this.oldestPerson = otherPerson;
    } else {
      this.youngestPerson = otherPerson;
      this.oldestPerson = onePerson;
    }
  }

  private void setAgeDifference() {
    this.ageDifference = oldestPerson.getBirthDate()
        .getTime() - youngestPerson.getBirthDate()
        .getTime();
  }

  public static List<PersonPair> fromPersonList(List<Person> personList) {
    List<PersonPair> pairList = new ArrayList<>();
    for (int i = 0; i < personList.size() - 1; i++) {
      for (int j = i + 1; j < personList.size(); j++) {
        pairList.add(new PersonPair(personList.get(i), personList.get(j)));
      }
    }
    return pairList;
  }
}
