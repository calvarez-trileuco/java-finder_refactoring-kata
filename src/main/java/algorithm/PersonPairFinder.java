package algorithm;

import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.getLast;
import static java.util.Comparator.comparingLong;
import static java.util.stream.Collectors.toList;

import java.util.List;

public class PersonPairFinder {

  private final List<Person> personList;

  public PersonPairFinder(List<Person> personList) {
    this.personList = personList;
  }

  public PersonPair find(Criteria criteria) {

    List<PersonPair> pairList = PersonPair.fromPersonList(personList)
        .stream()
        .sorted(comparingLong(PersonPair::getAgeDifference))
        .collect(toList());

    return criteria == Criteria.CLOSEST ? getFirst(pairList, new PersonPair()) : getLast(pairList, new PersonPair());
  }
}
