package algorithm;

import java.util.Date;

public class Person {

  public String name;

  public Date birthDate;

  public Date getBirthDate() {
    return birthDate;
  }

  public boolean isYoungerThan(Person anotherPerson) {
    return this.getBirthDate()
        .getTime() < anotherPerson.getBirthDate()
        .getTime();
  }
}

